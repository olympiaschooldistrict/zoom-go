module bitbucket.org/olympiaschooldistrict/zoom-go

go 1.14

require (
	github.com/gbrlsnchs/jwt/v3 v3.0.0-rc.2
	github.com/patrickmn/go-cache v2.1.0+incompatible
	go.uber.org/ratelimit v0.1.0 // indirect
)
