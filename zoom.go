package zoom

import (
	"net/http"
	"time"

	"github.com/gbrlsnchs/jwt/v3"
	"github.com/patrickmn/go-cache"
	"go.uber.org/ratelimit"
)

//All the things needed to interact with Zoom
type Service struct {
	client      http.Client
	conf        *Config
	authPayload jwt.Payload
	cache       *cache.Cache
	rate        map[string]ratelimit.Limiter
}

//API auth info
type Config struct {
	Key    string
	Secret []byte
}

//Setup a new service and test the connection
func NewService(config *Config) (s *Service, err error) {
	s = new(Service)
	s.cache = cache.New(5*time.Minute, 15*time.Minute)
	s.conf = config
	s.authPayload = jwt.Payload{
		Issuer: config.Key,
	}

	//test make a token
	_, err = s.token()
	if err != nil {
		return
	}

	s.rate = make(map[string]ratelimit.Limiter)

	s.rate["Light"] = ratelimit.New(80, ratelimit.WithoutSlack)             // 80 per second
	s.rate["Medium"] = ratelimit.New(60, ratelimit.WithoutSlack)            // 60 per second
	s.rate["Heavy"] = ratelimit.New(40, ratelimit.WithoutSlack)             // 40 per second 60,000/day
	s.rate["Resource-Intensive"] = ratelimit.New(1, ratelimit.WithoutSlack) // 20 per minute max 30,000/day
	return
}

//Make a new JWT token and sign it
func (s *Service) token() ([]byte, error) {
	s.authPayload.ExpirationTime = jwt.NumericDate(time.Now().Add(5 * time.Minute))
	s.authPayload.NotBefore = jwt.NumericDate(time.Now().Add(-30 * time.Minute))

	hs := jwt.NewHS256(s.conf.Secret)
	return jwt.Sign(s.authPayload, hs)
}

//Attach auth info to a request
func (s *Service) authify(r *http.Request) error {
	token, err := s.token()
	if err != nil {
		return err
	}
	r.Header.Set("Authorization", "Bearer "+string(token))
	r.Header.Set("content-type", "application/json")
	return nil
}
