package zoom

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

func (s *Service) ListGroupMembers(groupID string, pageNum int) ([]Member, error) {
	s.rate["Medium"].Take()
	request, err := http.NewRequest("GET", fmt.Sprintf("https://api.zoom.us/v2/groups/%s/members", groupID), nil)
	if err != nil {
		return nil, err
	}
	q := request.URL.Query()
	q.Set("page_size", "300")
	if pageNum != 1 {
		q.Set("page_number", strconv.Itoa(pageNum))
	}

	request.URL.RawQuery = q.Encode()

	err = s.authify(request)
	response, err := s.client.Do(request)
	if err != nil {
		return nil, err
	}

	res := listGroupMembersResponse{}
	bod, _ := ioutil.ReadAll(response.Body)
	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&res)
	if err != nil {
		return nil, err
	}
	var members = make([]Member, 0)
	members = append(members, res.Members...)

	if res.PageNumber < res.PageCount {
		nextSet, err := s.ListGroupMembers(groupID, res.PageNumber+1)
		if err != nil {
			return nil, err
		}
		members = append(members, nextSet...)
		return members, nil
	}
	return members, nil
}

type listGroupMembersResponse struct {
	OldPaginate
	Members []Member `json:"members"`
}

type Member struct {
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	ID        string `json:"id"`
	LastName  string `json:"last_name"`
	Type      int    `json:"type"`
}

func (s *Service) AddGroupMembers(groupID string, toAdd []string) error {
	s.rate["Medium"].Take()
	var nextChunk []string
	if len(toAdd) > 30 {
		nextChunk = toAdd[30:]
		toAdd = toAdd[:30]
	}
	body := struct {
		Members []struct {
			Id string `json:"id"`
		} `json:"members"`
	}{}

	for _, v := range toAdd {
		body.Members = append(body.Members, struct {
			Id string `json:"id"`
		}{
			Id: v,
		})
	}

	bodBytes, err := json.Marshal(body)
	if err != nil {
		return err
	}

	request, err := http.NewRequest("POST", fmt.Sprintf("https://api.zoom.us/v2/groups/%s/members", groupID), bytes.NewBuffer(bodBytes))
	if err != nil {
		return err
	}

	err = s.authify(request)
	response, err := s.client.Do(request)
	if err != nil && response != nil {
		return fmt.Errorf("http status: %v, error: %v", response.StatusCode, err)
	} else if err != nil {
		return err
	}
	if len(nextChunk) != 0 {
		err = s.AddGroupMembers(groupID, nextChunk)
		if err != nil {
			return err
		}
	}
	return nil
}
