package zoom

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/patrickmn/go-cache"
)

func (s *Service) getPastMeetings(nextPage string) ([]Meeting, error) {
	s.rate["Resource-Intensive"].Take()

	request, err := http.NewRequest("GET", "https://api.zoom.us/v2/metrics/meetings", nil)
	if err != nil {
		return nil, err
	}
	q := request.URL.Query()
	q.Set("from", time.Now().Add(-30*24*time.Hour).Format("2006-01-02"))
	q.Set("to", time.Now().Format("2006-01-02"))
	q.Set("page_size", "300")
	q.Set("type", "past")
	if nextPage != "" {
		q.Set("next_page_token", nextPage)
	}

	request.URL.RawQuery = q.Encode()

	err = s.authify(request)
	response, err := s.client.Do(request)
	if err != nil {
		return nil, err
	}

	res := metricsMeetingsResponse{}
	bod, _ := ioutil.ReadAll(response.Body)
	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&res)
	if err != nil {
		return nil, err
	}
	var meetings = make([]Meeting, 0)
	meetings = append(meetings, res.Meetings...)

	if res.NextPageToken != "" {
		nextSet, err := s.getPastMeetings(res.NextPageToken)
		if err != nil {
			return nil, err
		}
		meetings = append(meetings, nextSet...)
		return meetings, nil
	}
	return meetings, nil
}

//Get all past meetings form the cache or get fresh
func (s *Service) PastMeetings() ([]Meeting, error) {
	var cacheName = "dashboardMeetings"

	set, found := s.cache.Get(cacheName)
	if s, ok := set.([]Meeting); found && ok {
		return s, nil
	}

	fresh, err := s.getPastMeetings("")
	if err != nil {
		return nil, err
	}
	s.cache.Set(cacheName, fresh, cache.DefaultExpiration)
	return fresh, nil
}

func (s *Service) CurrentMeetings(nextPage string) ([]Meeting, error) {
	s.rate["Resource-Intensive"].Take()
	request, err := http.NewRequest("GET", "https://api.zoom.us/v2/metrics/meetings", nil)
	if err != nil {
		return nil, err
	}
	q := request.URL.Query()
	q.Set("from", time.Now().Add(-1*24*time.Hour).Format("2006-01-02"))
	q.Set("to", time.Now().Format("2006-01-02"))
	q.Set("page_size", "300")
	if nextPage != "" {
		q.Set("next_page_token", nextPage)
	}

	request.URL.RawQuery = q.Encode()

	err = s.authify(request)
	response, err := s.client.Do(request)
	if err != nil {
		return nil, err
	}

	res := metricsMeetingsResponse{}
	bod, _ := ioutil.ReadAll(response.Body)
	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&res)
	if err != nil {
		return nil, err
	}
	var meetings = make([]Meeting, 0)
	meetings = append(meetings, res.Meetings...)

	if res.NextPageToken != "" {
		nextSet, err := s.CurrentMeetings(res.NextPageToken)
		if err != nil {
			return nil, err
		}
		meetings = append(meetings, nextSet...)
		return meetings, nil
	}
	return meetings, nil
}

type metricsMeetingsResponse struct {
	From string `json:"from"`
	To   string `json:"to"`
	Paginate
	Meetings []Meeting `json:"meetings"`
}

//Represents a past meeting in zoom
type Meeting struct {
	UUID             string `json:"uuid"`
	ID               int    `json:"id"`
	Topic            string `json:"topic"`
	Host             string `json:"host"`
	Email            string `json:"email"`
	UserType         string `json:"user_type"`
	StartTime        string `json:"start_time"`
	EndTime          string `json:"end_time"`
	Duration         string `json:"duration"`
	Participants     int    `json:"participants"`
	HasPstn          bool   `json:"has_pstn"`
	HasVoip          bool   `json:"has_voip"`
	Has3RdPartyAudio bool   `json:"has_3rd_party_audio"`
	HasVideo         bool   `json:"has_video"`
	HasScreenShare   bool   `json:"has_screen_share"`
	HasRecording     bool   `json:"has_recording"`
	HasSip           bool   `json:"has_sip"`
}

func (m Meeting) String() string {
	return fmt.Sprintf("UUID: %v, ID:%v: Topic: %v, Host: %v, Duration: %v", m.UUID, m.ID, m.Topic, m.Host, m.Duration)
}

func (s *Service) MeetingAttendees(meetingID string, meetingType string, nextPage string) (participants []MeetingParticipant, err error) {
	s.rate["Resource-Intensive"].Take() //todo check this limit, it's not specified yet
	request, err := http.NewRequest("GET", fmt.Sprintf("https://api.zoom.us/v2/metrics/meetings/%v/participants", meetingID), nil)
	if err != nil {
		return nil, err
	}
	q := request.URL.Query()
	q.Set("page_size", "300")
	q.Set("type", meetingType)
	if nextPage != "" {
		q.Set("next_page_token", nextPage)
	}

	request.URL.RawQuery = q.Encode()

	err = s.authify(request)
	response, err := s.client.Do(request)
	if err != nil {
		return nil, err
	}
	if response.StatusCode == 429 {
		response, err = s.client.Do(request)
		if err != nil {
			return nil, err
		}
	}

	res := DashboardMeetingParticipantsResponse{}
	bod, _ := ioutil.ReadAll(response.Body)
	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&res)
	if err != nil {
		return nil, err
	}
	var users = make([]MeetingParticipant, 0)
	users = append(users, res.Participants...)

	if res.NextPageToken != "" {
		nextSet, err := s.MeetingAttendees(meetingID, meetingType, res.NextPageToken)
		if err != nil {
			return nil, err
		}
		participants = append(participants, nextSet...)
		return participants, nil
	}
	return users, nil
}

//Get all past meetings form the cache or get fresh
func (s *Service) CachedMeetingAttendees(meetingID string, meetingType string) ([]MeetingParticipant, error) {
	var cacheName = fmt.Sprintf("dashboardMeetingsAttendees-%v", meetingID)

	set, found := s.cache.Get(cacheName)
	if s, ok := set.([]MeetingParticipant); found && ok {
		return s, nil
	}

	fresh, err := s.MeetingAttendees(meetingID, meetingType, "")
	if err != nil {
		return nil, err
	}
	s.cache.Set(cacheName, fresh, cache.DefaultExpiration)
	return fresh, nil
}

type DashboardMeetingParticipantsResponse struct {
	Paginate
	Participants []MeetingParticipant `json:"participants"`
}

type MeetingParticipant struct {
	ID               string    `json:"id"`
	UserID           string    `json:"user_id"`
	UserName         string    `json:"user_name"`
	Device           string    `json:"device"`
	IPAddress        string    `json:"ip_address"`
	Location         string    `json:"location"`
	NetworkType      string    `json:"network_type"`
	Microphone       string    `json:"microphone"`
	Speaker          string    `json:"speaker"`
	DataCenter       string    `json:"data_center"`
	ConnectionType   string    `json:"connection_type"`
	JoinTime         time.Time `json:"join_time"`
	LeaveTime        time.Time `json:"leave_time"`
	ShareApplication bool      `json:"share_application"`
	ShareDesktop     bool      `json:"share_desktop"`
	ShareWhiteboard  bool      `json:"share_whiteboard"`
	Recording        bool      `json:"recording"`
	PcName           string    `json:"pc_name"`
	Domain           string    `json:"domain"`
	MacAddr          string    `json:"mac_addr"`
	HarddiskID       string    `json:"harddisk_id"`
	Version          string    `json:"version"`
	LeaveReason      string    `json:"leave_reason"`
}
