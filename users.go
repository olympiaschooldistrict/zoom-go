package zoom

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/patrickmn/go-cache"
)

func (s *Service) listUsers(pageNum int) ([]User, error) {
	s.rate["Medium"].Take()
	request, err := http.NewRequest("GET", "https://api.zoom.us/v2/users", nil)
	if err != nil {
		return nil, err
	}
	q := request.URL.Query()
	q.Set("page_size", "300")
	if pageNum != 1 {
		q.Set("page_number", strconv.Itoa(pageNum))
	}

	request.URL.RawQuery = q.Encode()

	err = s.authify(request)
	response, err := s.client.Do(request)
	if err != nil {
		return nil, err
	}
	if response.StatusCode == 429 {
		response, err = s.client.Do(request)
		if err != nil {
			return nil, err
		}
	}

	res := listUsersResponse{}
	bod, _ := ioutil.ReadAll(response.Body)
	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&res)
	if err != nil {
		return nil, err
	}
	var users = make([]User, 0)
	users = append(users, res.Users...)
	if res.PageNumber < res.PageCount {
		nextSet, err := s.listUsers(res.PageNumber + 1)
		if err != nil {
			return nil, err
		}
		users = append(users, nextSet...)
		return users, nil
	}
	return users, nil
}

func (s *Service) GetUsers() ([]User, error) {
	var cacheName = "users"

	set, found := s.cache.Get(cacheName)
	if s, ok := set.([]User); found && ok {
		return s, nil
	}

	fresh, err := s.listUsers(1)
	if err != nil {
		return nil, err
	}
	s.cache.Set(cacheName, fresh, cache.DefaultExpiration)
	return fresh, nil
}

type listUsersResponse struct {
	OldPaginate
	Users []User `json:"users"`
}

type User struct {
	ID                string    `json:"id"`
	FirstName         string    `json:"first_name"`
	LastName          string    `json:"last_name"`
	Email             string    `json:"email"`
	Type              int       `json:"type"`
	Pmi               int       `json:"pmi"`
	Timezone          string    `json:"timezone"`
	Verified          int       `json:"verified"`
	Dept              string    `json:"dept"`
	CreatedAt         time.Time `json:"created_at"`
	LastLoginTime     string    `json:"last_login_time"`
	LastClientVersion string    `json:"last_client_version"`
	PicURL            string    `json:"pic_url"`
	ImGroupIds        []string  `json:"im_group_ids"`
	Status            string    `json:"status"`
}

func (u User) String() string {
	return fmt.Sprintf("%v: %v %v, Email: %v, Type: %v", u.ID, u.FirstName, u.LastName, u.Email, u.Type)
}

type UserToCreate struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Type      int    `json:"type"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

func (s *Service) CreateUsers(action string, users []UserToCreate) error {
	s.rate["Light"].Take()

	//I think this endpoint only wants one? Maybe 30
	var nextChunk []UserToCreate
	if len(users) > 1 {
		nextChunk = users[1:]
		users = users[:1]
	}
	body := struct {
		Action string       `json:"action"`
		User   UserToCreate `json:"user_info"`
	}{
		Action: action,
		User:   users[0],
	}
	bodBytes, err := json.Marshal(body)
	if err != nil {
		return err
	}
	request, err := http.NewRequest("POST", fmt.Sprintf("https://api.zoom.us/v2/users"), bytes.NewBuffer(bodBytes))
	if err != nil {
		return err
	}

	err = s.authify(request)
	response, err := s.client.Do(request)
	if err != nil && response != nil {
		return fmt.Errorf("http status: %v, error: %v", response.StatusCode, err)
	} else if err != nil {
		return err
	}
	if response.StatusCode != 201 {
		bod, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return err
		}
		fmt.Println(fmt.Errorf("create user failed, status: %v, response: %v, bod: %v", response.StatusCode, string(bod), string(bodBytes)))
		//return fmt.Errorf("create user failed, status: %v, response: %v, bod: %v", response.StatusCode, string(bod), string(bodBytes))
	}
	if len(nextChunk) != 0 {
		err = s.CreateUsers(action, nextChunk)
		if err != nil {
			return err
		}
	}
	return nil
}

type UpdateUserBody struct {
	FirstName    string `json:"first_name,omitempty"`
	LastName     string `json:"last_name,omitempty"`
	Type         string `json:"type,omitempty"`
	Pmi          string `json:"pmi,omitempty"`
	Timezone     string `json:"timezone,omitempty"`
	Dept         string `json:"dept,omitempty"`
	VanityName   string `json:"vanity_name,omitempty"`
	HostKey      string `json:"host_key,omitempty"`
	CmsUserID    string `json:"cms_user_id,omitempty"`
	JobTitle     string `json:"job_title,omitempty"`
	Company      string `json:"company,omitempty"`
	Location     string `json:"location,omitempty"`
	PhoneNumber  string `json:"phone_number,omitempty"`
	PhoneCountry string `json:"phone_country,omitempty"`
}

func (s *Service) UpdateUser(uid string, u UpdateUserBody) error {
	s.rate["Light"].Take()
	bodBytes, err := json.Marshal(u)
	if err != nil {
		return err
	}
	request, err := http.NewRequest("PATCH", fmt.Sprintf("https://api.zoom.us/v2/users/%s", uid), bytes.NewBuffer(bodBytes))
	if err != nil {
		return err
	}

	err = s.authify(request)
	response, err := s.client.Do(request)
	if err != nil && response != nil {
		return fmt.Errorf("http status: %v, error: %v", response.StatusCode, err)
	} else if err != nil {
		return err
	}
	if response.StatusCode != 204 {
		bod, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return err
		}
		fmt.Println(fmt.Errorf("update user failed, status: %v, response: %v, bod: %v", response.StatusCode, string(bod), string(bodBytes)))
		//return fmt.Errorf("create user failed, status: %v, response: %v, bod: %v", response.StatusCode, string(bod), string(bodBytes))
	}

	return nil
}
