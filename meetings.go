package zoom

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
)

func (s *Service) EndMeeting(meetingID string) error {
	s.rate["Light"].Take()
	bodBytes := []byte(`{"action": "end"}`)
	request, err := http.NewRequest("PUT", fmt.Sprintf("https://api.zoom.us/v2/meetings/%s/status", meetingID), bytes.NewBuffer(bodBytes))
	if err != nil {
		return err
	}

	err = s.authify(request)
	response, err := s.client.Do(request)
	if err != nil {
		return err
	}
	if response.StatusCode != 204 {
		bod, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return err
		}
		return fmt.Errorf("ending meeting failed, status: %v, response: %v", response.StatusCode, string(bod))
	}
	return nil
}
